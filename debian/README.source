python-uflash
=============

Obtaining the source
--------------------

The uflash source is obtained from Github. At the time of writing upstream
do not tag releases, so a tarball of the latest release is manually downloaded
to e.g. uflash-1.2.4.tar.gz for repacking. A watch file is provided so that new
releases can be automatically downloaded and repacked when tags are available.


Repacking the source
--------------------

The upstream source is repacked to remove non-DFSG files:

 * a pre-compiled MicroPython runtime, firmware.hex
 * a copy of the stringified runtime in uflash.py

A repacking script is provided in debian/repack.sh

To repack an upstream tarball downloaded from Github, use the following:

  $ sh debian/repack.sh --upstream-source 1.2.4 ../uflash-1.2.4.tar.gz

This will repack the upstream tarball and generate a new DFSG-compatible
source tarball in ../python-uflash_1.2.4+dfsg.orig.tar.gz ready for
importing with `gbp import-orig ...`


Obtaining the MicroPython runtime
---------------------------------

The python3-uflash package depends on the firmware-microbit-micropython
package that provides a MicroPython runtime built from source. It is installed
to /usr/share/firmware-microbit-micropython/firmware.hex.


 -- Nick Morrott <nickm@debian.org>  Tue, 15 Feb 2022 10:38:09 +0000
